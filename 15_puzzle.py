# coding: utf-8
import pilasengine
from random import random

pilas = pilasengine.iniciar()

class Pieza(pilasengine.actores.Actor):
    def iniciar(self, numero):
        imagen = pilas.imagenes.cargar_superficie(50, 50)
        imagen.pintar(pilas.colores.amarillo)
        imagen.texto(numero, 15, 12, color = pilas.colores.negro, magnitud = 18)
        self.imagen = imagen

class Puzzle(pilasengine.actores.Actor):
    def iniciar(self):
        self.imagen = ''
        self.tamanio_grilla = 55
        self.piezas = []
        
        self.crear_tablero()
        self.desordenar(25)
        self.en_juego = True
        
        pilas.eventos.pulsa_tecla.conectar(self.al_pulsar)
    
    def crear_tablero(self):
        self.tablero = [[1,2,3,4],
                        [5,6,7,8],
                        [9,10,11,12],
                        [13,14,15,0]]
        for i, fila in enumerate(self.tablero):
            for j, numero in enumerate(fila):
                if numero > 0:
                    pieza = pilas.actores.Pieza(numero = str(numero))
                    pieza.x = self.tamanio_grilla * j - self.tamanio_grilla * 1.5
                    pieza.y = -self.tamanio_grilla * i + self.tamanio_grilla * 1.5
                    self.piezas.append(pieza)
    
    def al_pulsar(self, evento):
        if self.en_juego:
            self.mover_pieza(evento.codigo)
            if self.verificar_victoria():
                print('¡¡¡¡¡¡¡ V I C T O R I A !!!!!!!')
                self.en_juego = False
    
    def mover_pieza(self, direccion):
        valido = False
        x, y = self.busco_vacio()
        if direccion == pilas.simbolos.ABAJO:
            if y > 0:
                valido = True
                pieza = self.tablero[y-1][x]
                self.tablero[y-1][x] = 0
                self.tablero[y][x] = pieza
                self.piezas[pieza-1].y -= self.tamanio_grilla
        elif direccion == pilas.simbolos.ARRIBA:
            if y < 3:
                valido = True
                pieza = self.tablero[y+1][x]
                self.tablero[y+1][x] = 0
                self.tablero[y][x] = pieza
                self.piezas[pieza-1].y += self.tamanio_grilla
        elif direccion == pilas.simbolos.DERECHA:
            if x > 0:
                valido = True
                pieza = self.tablero[y][x-1]
                self.tablero[y][x-1] = 0
                self.tablero[y][x] = pieza
                self.piezas[pieza-1].x += self.tamanio_grilla
        elif direccion == pilas.simbolos.IZQUIERDA:
            if x < 3:
                valido = True
                pieza = self.tablero[y][x+1]
                self.tablero[y][x+1] = 0
                self.tablero[y][x] = pieza
                self.piezas[pieza-1].x -= self.tamanio_grilla
        return valido
            
    def busco_vacio(self):
        for y, fila in enumerate(self.tablero):
            for x, numero in enumerate(fila):
                if numero == 0:
                    return (x, y)
    
    def desordenar(self, movimientos):
        seguridad = 1000
        direcciones = (pilas.simbolos.ARRIBA,
                       pilas.simbolos.ABAJO,
                       pilas.simbolos.DERECHA,
                       pilas.simbolos.IZQUIERDA)
        while movimientos > 0 and seguridad > 0:
            seguridad -= 1
            if self.mover_pieza(direcciones[int(4 * random())]):
                movimientos -= 1
    
    def verificar_victoria(self):
        victoria = True
        numero_orden = 1
        for y, fila in enumerate(self.tablero):
            for x, numero in enumerate(fila):
                if numero_orden <= 15 and numero != numero_orden:
                    victoria = False
                numero_orden += 1
        return victoria
        
pilas.fondos.Color(pilas.colores.negro)

pilas.actores.vincular(Pieza)
pilas.actores.vincular(Puzzle)

pilas.actores.Puzzle()
            
pilas.ejecutar()