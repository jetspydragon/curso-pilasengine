# coding: utf-8
import pilasengine
import random
import math

# Setup del juego
pilas = pilasengine.iniciar()
rnd = random.Random()

AA = dict()    
(AA['x1'], AA['x2'], AA['y1'], AA['y2']) = pilas.camara.obtener_area_visible()    

# Actores
class Jugador(pilasengine.actores.Actor):
    def iniciar(self, x = 0, y = 0):
        self.imagen = 'assets/jugador.png'
        self.x = x
        self.y = y
        self.es_ia = True
    
    def actualizar(self):
        if self.es_ia:
            self.actualizar_ia()
        else:
            self.actualizar_jugador()
    
    def actualizar_jugador(self):
        (mx, my) = pilas.obtener_posicion_del_mouse()
        self.y = my
        
    def actualizar_ia(self):
        pass

class Bola(pilasengine.actores.Actor):
    def iniciar(self):
        self.imagen = 'assets/bola.png'
        self.reset()
                
    def actualizar(self):
        self.x += self.velocidadX
        self.y += self.velocidadY
        
        # Si la bola sale por los extremos laterales
        # ésta se resetea
        if (self.x > AA['x2'] or self.x < AA['x1']):
            self.reset()
            
        # Si la bola llega a las bordes superiores e inferiores
        # invierte su velocidad vertical 
        if (self.y > AA['y1']):
            self.y = AA['y1']
            self.velocidadY = -self.velocidadY
        elif (self.y < AA['y2']):
            self.y = AA['y2']
            self.velocidadY = -self.velocidadY
    
    def reset(self):
        self.x = 0
        self.y = 0
        # Obtengo un ángulo de partida
        angulo = math.floor(rnd.random() * 13) * 10 - 240
        modulo = rnd.random() * 4 + 4
        pilas.avisar('Angulo ' + str(angulo) + ' - Modulo ' + str(modulo))
        self.velocidadX = modulo * math.cos(math.radians(angulo))
        self.velocidadY = modulo * math.sin(math.radians(angulo))
                
# Escenas
class Juego(pilasengine.escenas.Escena):
    def iniciar(self):
        pilas.fondos.Color(pilas.colores.grisoscuro)
        
        self.jugador1 = Jugador(pilas, AA['x1'] * 0.95)
        self.jugador1.es_ia = False
        self.jugador2 = Jugador(pilas, AA['x2'] * 0.95)
        self.bola = Bola(pilas)
    
    def actualizar(self):
        pass        
        
pilas.escenas.vincular(Juego)
pilas.escenas.Juego()
pilas.ejecutar()
