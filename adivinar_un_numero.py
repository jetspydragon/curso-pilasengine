# coding: utf-8
import pilasengine
from random import random 

# Defino variables
pilas = pilasengine.iniciar()
numero_pensado = int(50 * random() + 1)

# Defino los actores
pizarra = pilas.actores.Pizarra()
texto_ingresado = pilas.interfaz.IngresoDeTexto('')
texto_ingresado.solo_numeros()
boton = pilas.interfaz.Boton('Adivinar')
mono = pilas.actores.Mono()

# Aplico propiedades
pilas.fondos.Color(pilas.colores.negro)
boton.escala = 1.2
pizarra.texto(u'Pienso un número del 1 al 50, adivine:', 
    -140, 80, color = pilas.colores.blanco, magnitud = 12)
texto_ingresado.y = 40
mono.y = -100

def blanquear_texto():
    texto_ingresado.texto = ''
    texto_ingresado.obtener_foco()

def adivinar():
    numero = int(texto_ingresado.texto)
    if numero == numero_pensado:
        mono.decir(u'Es correcto!!')
    elif numero_pensado > numero:
        mono.decir(u'El número que pienso es mayor')
    else:
        mono.decir(u'El número que pienso es menor')
    pilas.tareas.una_vez(1, blanquear_texto)

boton.conectar(adivinar)
blanquear_texto()

pilas.ejecutar()