# coding: utf-8
import pilasengine

pilas = pilasengine.iniciar()

pizarra = pilas.actores.Pizarra()
pizarra.pintar(pilas.colores.azul)

pizarra.rectangulo(-75, 75, 150, 150, relleno = True, color = pilas.colores.gris)
        
pizarra.linea(-100, 100, 100, 100, color = pilas.colores.blanco, grosor = 3)
pizarra.linea(-100, -100, 100, -100, color = pilas.colores.blanco, grosor = 3)
pizarra.dibujar_punto(0, 0, color = pilas.colores.blanco)

pizarra.poligono([(-50, -50),
                  (50, -50),
                  (0, 50),
                  (-50, -50)],
                  color = pilas.colores.blanco,
                  grosor = 3)

pizarra.poligono([(50, 50),
                  (-50, 50),
                  (0, -50),
                  (50, 50)],
                  color = pilas.colores.blanco,
                  grosor = 3)
                    
# Borra todo lo dibujado
# pizarra.limpiar()

pilas.ejecutar()