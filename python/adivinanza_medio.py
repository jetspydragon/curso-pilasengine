from random import random

NUMERO_MAXIMO = 50
numero_pensado = int(NUMERO_MAXIMO * random()) + 1
numero_adivinado = -1
intentos = 0

while (numero_pensado != numero_adivinado):
    intentos += 1
    numero_adivinado = int(input('Pienso un n�mero del 1 al ' +
                                 str(NUMERO_MAXIMO) + ': '))
    if numero_adivinado == numero_pensado:
        print('Has ganado!!! El n�mero era el ' + str(numero_pensado) +
              '. Has adivinado en ' + str(intentos) + ' intentos.')
    elif numero_adivinado > numero_pensado:
        print('El numero que pienso es menor.')
    else:
        print('El numero que pienso es mayor.')
