# coding: utf-8
import pilasengine
from random import random

pilas = pilasengine.iniciar()
pilas.fondos.Color(pilas.colores.negro)

class Boton_Simon(pilasengine.actores.Actor):
    def iniciar(self, color = pilas.colores.rojo):
        imagen = pilas.imagenes.cargar_superficie(100, 100)
        imagen.circulo(50, 50, 50, color = color, relleno = True)
        self.transparencia = 40
        self.imagen = imagen
        self.sonido = ''
    
    def boton_encendido(self):
        self.transparencia = 0
        if self.sonido:
            self.sonido.reproducir()
        
    def boton_apagado(self):
        self.transparencia = 40
        if self.sonido:
            self.sonido.detener()
    
    def presionar(self):
        self.boton_encendido()
        pilas.tareas.una_vez(0.3, self.boton_apagado) 
    
    def punto_en_area(self, x, y):
        return (y > self.abajo and y < self.arriba and 
                x > self.izquierda and x < self.derecha)
        
        
class Juego_Simon(pilasengine.actores.Actor):
    def iniciar(self):
        self.imagen = ''
        self.y = 100
        self.botones = (pilas.actores.Boton_Simon(color = pilas.colores.rojo),
                        pilas.actores.Boton_Simon(color = pilas.colores.azul),
                        pilas.actores.Boton_Simon(color = pilas.colores.amarillo),
                        pilas.actores.Boton_Simon(color = pilas.colores.verde))

        self.botones[0].x = self.x - 60
        self.botones[0].y = self.y + 60
        self.botones[0].sonido = pilas.sonidos.cargar('assets\E4.ogg')
        self.botones[1].x = self.x + 60
        self.botones[1].y = self.y + 60
        self.botones[1].sonido = pilas.sonidos.cargar('assets\C#4.ogg')
        self.botones[2].x = self.x - 60
        self.botones[2].y = self.y - 60
        self.botones[2].sonido = pilas.sonidos.cargar('assets\A4.ogg')
        self.botones[3].x = self.x + 60
        self.botones[3].y = self.y - 60
        self.botones[3].sonido = pilas.sonidos.cargar('assets\E5.ogg')
            
        self.secuencia = []
        self.errores = 0
        self.estado = ''
        
        pilas.actores.Texto("Mostrando", y = -50, x = -50)
        self.boton_mostrando = pilas.actores.Boton_Simon(color = pilas.colores.rojo)
        self.boton_mostrando.escala = 0.3
        self.boton_mostrando.y = -50
        self.boton_mostrando.x = 90
        
        self.texto_largo = pilas.actores.Texto(self.obtener_texto_largo())
        self.texto_largo.y = -90
        
        self.texto_errores = pilas.actores.Texto(self.obtener_texto_errores())
        self.texto_errores.y = -130
        
        self.boton_iniciar = pilas.interfaz.Boton('Iniciar')
        self.boton_iniciar.y = -180
        self.boton_iniciar.x = -100
        self.boton_iniciar.escala = 1.5
        self.boton_iniciar.conectar(self.iniciar_juego)
        
        self.boton_reiniciar = pilas.interfaz.Boton('Reiniciar')
        self.boton_reiniciar.y = -180
        self.boton_reiniciar.x = 100
        self.boton_reiniciar.escala = 1.5
        self.boton_reiniciar.conectar(self.reiniciar_juego)
        self.boton_reiniciar.transparencia = 100   
        self.boton_reiniciar.activo = False  
        
        self.click_de_mouse(self.al_tocar)
        
        self.estado_inicial()
        
    def estado_inicial(self):
        print('Estado inicial...')
        for boton in self.botones:
            boton.boton_apagado()
            
        self.secuencia = [self.obtener_siguiente(),
                          self.obtener_siguiente(),
                          self.obtener_siguiente()]
        self.errores = 0
        self.estado = ''
        
    def actualizar(self):
        pass
        
    def obtener_siguiente(self):
        return int(4 * random())
    
    def obtener_texto_largo(self):
        return 'Largo         ' + str(len(self.secuencia))
    
    def obtener_texto_errores(self):
        return 'Errores       ' + str(self.errores)
    
    def iniciar_juego(self):
        print('Iniciar juego...')
        self.boton_reiniciar.transparencia = 0   
        self.boton_reiniciar.activo = True  
        self.boton_iniciar.transparencia = 100   
        self.boton_iniciar.activo = False      
        self.estado_mostrar()
        
    def reiniciar_juego(self):
        if self.estado == 'Jugando':
            print('Reiniciar juego...')
            self.boton_iniciar.transparencia = 0   
            self.boton_iniciar.activo = True  
            self.boton_reiniciar.transparencia = 100   
            self.boton_reiniciar.activo = False  
        
            self.estado_inicial()
        else:
            print('Sólo puede reiniciar cuándo es su turno.')
        
    def estado_mostrar(self):
        self.cambiar_estado('Mostrando')
        self.boton_mostrando.boton_encendido()
        self.orden_mostrar = 0
        self.texto_largo.texto = self.obtener_texto_largo()
        self.texto_errores.texto = self.obtener_texto_errores()
        self.mostrar_secuencia()
        
    def mostrar_secuencia(self):
        for boton in self.botones:
            boton.boton_apagado()
        self.botones[self.secuencia[self.orden_mostrar]].boton_encendido()
        self.orden_mostrar += 1
        if self.orden_mostrar >= len(self.secuencia):
            pilas.tareas.una_vez(0.8, self.apagar_actual, True)
        else:
            pilas.tareas.una_vez(0.8, self.apagar_actual, False)
            pilas.tareas.una_vez(1, self.mostrar_secuencia)
    
    def apagar_actual(self, fin_secuencia):
        self.botones[self.secuencia[self.orden_mostrar-1]].boton_apagado()
        if fin_secuencia:
            self.boton_mostrando.boton_apagado()
            self.estado_jugar()
    
    def estado_jugar(self):
        self.cambiar_estado('Jugando')
        self.orden_jugar = 0
    
    def al_tocar(self, evento):
        if self.estado == 'Jugando':
            self.cambiar_estado('Evaluando')
            # Reviso que botón fue tocado
            for orden, boton in enumerate(self.botones):
                if boton.punto_en_area(evento.x, evento.y):
                    # Realizo el efecto de presión sólo si es el correcto
                    if orden == self.secuencia[self.orden_jugar]:
                        boton.presionar()
                        self.orden_jugar += 1
                        if self.orden_jugar >= len(self.secuencia):
                            self.estado == 'FinJugar'
                            pilas.tareas.una_vez(1, self.fin_estado_jugar)
                        else:
                            self.cambiar_estado('Jugando')
                    else:
                        self.errores += 1
                        self.cambiar_estado('Jugando')
                        print('Botón incorrecto')

    def fin_estado_jugar(self):
        self.secuencia.append(self.obtener_siguiente())
        self.estado_mostrar()
    
    def cambiar_estado(self, estado):
        self.estado = estado
        print(self.estado)

pilas.actores.vincular(Boton_Simon)
pilas.actores.vincular(Juego_Simon)

juego = pilas.actores.Juego_Simon()

pilas.ejecutar()