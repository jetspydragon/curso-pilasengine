# coding: utf-8
'''
TODO
    - Condición de victoria
    - Botón para reiniciar el juego
'''
import pilasengine
from random import random 

# Defino variables
pilas = pilasengine.iniciar()
palabras = ('casa',
            'perro',
            'dentista',
            'psicologo',
            'pandilla',
            'espacio')
palabra_actual = palabras[int(len(palabras) * random())].upper()
palabra_mostrar = ''
letras_elegidas = ''
intentos = 0
fin_del_juego = False

horca_img = pilas.imagenes.cargar_superficie(100,100)
cabeza_img = pilas.imagenes.cargar_superficie(30,30)
cuerpo_img = pilas.imagenes.cargar_superficie(10,60)

# Dibujo las superficies
horca_img.rectangulo(25, 0, 100, 10, color = pilas.colores.marron, 
    relleno = True)
horca_img.rectangulo(90, 0, 10, 20, color = pilas.colores.marron, 
    relleno = True)
horca_img.rectangulo(25, 0, 10, 100, color = pilas.colores.marron, 
    relleno = True)
horca_img.rectangulo(20, 70, 20, 10, color = pilas.colores.marron, 
    relleno = True)
horca_img.rectangulo(10, 80, 40, 10, color = pilas.colores.marron, 
    relleno = True)
horca_img.rectangulo(0, 90, 60, 10, color = pilas.colores.marron, 
    relleno = True)
cabeza_img.circulo(15, 15, 15, relleno = True)
cuerpo_img.pintar(pilas.colores.negro)

# Defino botones de letras
class Boton_Letra(pilasengine.actores.Actor):
    def iniciar(self, texto_boton):
        self.imagen = ''
        self.boton = pilas.interfaz.Boton(texto_boton)
        self.boton.conectar(self.al_tocar)
        
    def al_tocar(self):
        global fin_del_juego
        if not fin_del_juego:
            self.boton.transparencia = 100
            elegir_letra(self.boton.texto)
    
pilas.actores.vincular(Boton_Letra)        
            
fila = 200
columna = -290
for indice, letra in enumerate(u'ABCDEFGHIJKLMNÑOPQRSTUVWXYZ'):
    btn = pilas.actores.Boton_Letra(texto_boton = letra)
    btn.boton.escala = 1.3
    btn.boton.x = columna + ((indice % 14) * 45)
    btn.boton.y = fila - (int(indice / 14) * 45)

# Defino actores    
horca = pilas.actores.Actor(imagen = horca_img)
cabeza = pilas.actores.Actor(imagen = cabeza_img)
cuerpo = pilas.actores.Actor(imagen = cuerpo_img)
brazo_izq = pilas.actores.Actor(imagen = cuerpo_img)
brazo_der = pilas.actores.Actor(imagen = cuerpo_img)
pierna_izq = pilas.actores.Actor(imagen = cuerpo_img)
pierna_der = pilas.actores.Actor(imagen = cuerpo_img)
hombre = (cabeza, 
          cuerpo, 
          brazo_izq, 
          brazo_der,
          pierna_izq,
          pierna_der)
pizarra = pilas.actores.Pizarra()

# Establezco propiedades
pilas.fondos.Tarde()
horca.escala = 2.5
horca.y = -115
horca.x = -50
cabeza.x = 62
cabeza.y = -60
cuerpo.x = 62
cuerpo.y = -100
brazo_izq.rotacion = 45
brazo_izq.x = 80
brazo_izq.y = -90
brazo_der.rotacion = -45
brazo_der.x = 62-18
brazo_der.y = -90
pierna_izq.rotacion = 45
pierna_izq.y = -140
pierna_izq.x = 80
pierna_der.rotacion = -45
pierna_der.y = -140
pierna_der.x = 62-18
for parte in hombre:
    parte.transparencia = 100

def elegir_letra(letra):
    global letras_elegidas
    global intentos
    global fin_del_juego
    letras_elegidas += letra
    palabra_mostrar = ''
    acierto = False    
    for letra_palabra in palabra_actual:
        encontrada = False
        for letra_elegida in letras_elegidas:
            if letra_palabra == letra_elegida:
                palabra_mostrar += letra_palabra + ' '
                encontrada = True
        if not encontrada:
            palabra_mostrar += '_ '
        if letra == letra_palabra:
            acierto = True
    if letra != '' and not acierto:
        hombre[intentos].transparencia = 0
        intentos += 1
    if intentos >= len(hombre):
        fin_del_juego = True
        print('Fin del juego')
    pizarra.limpiar()
    pizarra.texto('Palabra: ' + palabra_mostrar, -200, 100, magnitud = 20)

elegir_letra('')

pilas.ejecutar()