# coding: utf-8
import pilasengine

MOVIL_VELOCIDAD = 2
TAMANIO_TILE = 25

pilas = pilasengine.iniciar()

class Objetivo(pilasengine.actores.Actor):
    
    objetivos = []
    
    def iniciar(self):
        img = pilas.imagenes.cargar_superficie(TAMANIO_TILE, TAMANIO_TILE)
        img.rectangulo(10, 5, 4, 15, color = pilas.colores.blanco, relleno = True)
        self.imagen = img
        self.figura_de_colision = pilas.fisica.Rectangulo(1, 1, TAMANIO_TILE-2, TAMANIO_TILE-2, sensor=True, dinamica=False)
        self.z = -10
        
        Objetivo.objetivos.append(self)
    
    def eliminar(self):
        Objetivo.objetivos.remove(self)
        super(Objetivo, self).eliminar()
    
    @staticmethod
    def eliminar_todos():
        while len(Objetivo.objetivos) > 0:
            Objetivo.objetivos[0].eliminar()
        
class Muro(pilasengine.actores.Actor):
    
    muros = []
    
    def iniciar(self, color = pilas.colores.negro):
        img = pilas.imagenes.cargar_superficie(TAMANIO_TILE, TAMANIO_TILE)
        img.pintar(color)
        self.imagen = img
        self.figura_de_colision = pilas.fisica.Rectangulo(1, 1, TAMANIO_TILE-2, TAMANIO_TILE-2, sensor=True, dinamica=False)
        
        Muro.muros.append(self)
    
    def eliminar(self):
        Muro.muros.remove(self)
        super(Muro, self).eliminar()
    
    @staticmethod
    def eliminar_todos():
        while len(Muro.muros) > 0:
            Muro.muros[0].eliminar()
    
class Movil(pilasengine.actores.Actor):
  
    tag = 0
    moviles = []
    estado = 'quieto'
    
    def iniciar(self):
        img = pilas.imagenes.cargar_superficie(TAMANIO_TILE, TAMANIO_TILE)
        img.pintar(pilas.colores.negro)
        img.rectangulo(5, 10, 15, 4, color = pilas.colores.blanco, relleno = True)
        self.imagen = img
        self.estado = ''
        self.velocidad_x = 0
        self.velocidad_y = 0
        Movil.tag += 1
        self.id = Movil.tag
        
        self.cambiar_estado('quieto')
        self.figura_de_colision = pilas.fisica.Rectangulo(1, 1, TAMANIO_TILE-2, TAMANIO_TILE-2, sensor=True, dinamica=False)
        
        pilas.eventos.pulsa_tecla.conectar(self.al_pulsar)
        # Uso etiquetas para establecer las colisiones
        pilas.colisiones.agregar('movil', 'muro', self.al_chocar)
        pilas.colisiones.agregar('movil', 'movil', self.al_chocar)
        
        Movil.moviles.append(self)
        
    def actualizar(self):
        self.x = self.x + self.velocidad_x
        self.y = self.y + self.velocidad_y    
        pilas.colisiones.actualizar()
    
    def al_chocar(self, obj1, obj2):
        if isinstance(obj1, Movil) and isinstance(obj2, Movil):
            if obj1.estado == 'quieto' or obj2.estado == 'quieto':
                self.detenerse()
                self.cambiar_estado('quieto')
        elif obj1.id == self.id:
            self.detenerse()
            self.cambiar_estado('quieto')
    
    def detenerse(self):
        self.velocidad_x = 0
        self.velocidad_y = 0
        if self.x % TAMANIO_TILE < TAMANIO_TILE / 2:
            self.x = (self.x / TAMANIO_TILE) * TAMANIO_TILE
        else:
            self.x = ((self.x / TAMANIO_TILE) + 1) * TAMANIO_TILE
        if self.y % TAMANIO_TILE < TAMANIO_TILE / 2:
            self.y = (self.y / TAMANIO_TILE) * TAMANIO_TILE
        else:
            self.y = ((self.y / TAMANIO_TILE) + 1) * TAMANIO_TILE
                        
    def mover_arriba(self):
        self.velocidad_x = 0
        self.velocidad_y = MOVIL_VELOCIDAD
    
    def mover_abajo(self):
        self.velocidad_x = 0
        self.velocidad_y = -MOVIL_VELOCIDAD
        
    def mover_derecha(self):
        self.velocidad_x = MOVIL_VELOCIDAD
        self.velocidad_y = 0
        
    def mover_izquierda(self):
        self.velocidad_x = -MOVIL_VELOCIDAD
        self.velocidad_y = 0
    
    def al_pulsar(self, evento):
        if Movil.estado == 'quieto':
            if evento.codigo == pilas.simbolos.ABAJO:
                self.cambiar_estado('movimiento')
                self.mover_abajo()
            elif evento.codigo == pilas.simbolos.ARRIBA:
                self.cambiar_estado('movimiento')
                self.mover_arriba()
            elif evento.codigo == pilas.simbolos.DERECHA:
                self.cambiar_estado('movimiento')
                self.mover_derecha()
            elif evento.codigo == pilas.simbolos.IZQUIERDA:
                self.cambiar_estado('movimiento')
                self.mover_izquierda()
    
    def cambiar_estado(self, estado):
        self.estado = estado
        self.cambiar_estado_clase(estado)
        
        if self.estado == 'quieto':
            # Evalúo si estoy encima de un Objetivo
            for objetivo in Objetivo.objetivos:
                if self.x == objetivo.x and self.y == objetivo.y:
                    pilas.tareas.una_vez(1, NulGame.evaluar_victoria)
                    muro = pilas.actores.Muro()
                    muro.y = self.y
                    muro.x = self.x
                    objetivo.eliminar()
                    self.eliminar()
    
    def cambiar_estado_clase(self, estado):
        # Si están todos en <estado> cambio el estado de la clase
        cantidad = 0
        for movil in Movil.moviles:
            if movil.estado <> estado:
                cantidad += 1
        if cantidad == 0:
            Movil.estado = estado
    
    def eliminar(self):
        Movil.moviles.remove(self)
        super(Movil, self).eliminar()
    
    @staticmethod
    def eliminar_todos():
        while len(Movil.moviles) > 0:
            Movil.moviles[0].eliminar()
                    
class NulGame():
    
    nivel = 0
    
    @staticmethod
    def cargar_nivel():
        min_x = 999
        max_x = -1
        min_y = 999
        max_y = -1
        nivel = Nivel.niveles[NulGame.nivel]
        fondo = pilas.fondos.Color(nivel['color_fondo'])
        for y, cadena in enumerate(nivel['nivel']):
            for x, caracter in enumerate(cadena):
                objeto = None
                if caracter == '#':
                    objeto = pilas.actores.Muro(color = nivel['color'])
                elif caracter == '+':
                    objeto = pilas.actores.Movil()
                elif caracter == '-':
                    objeto = pilas.actores.Objetivo()
                if objeto:
                    objeto.x = x * TAMANIO_TILE
                    objeto.y = -y * TAMANIO_TILE

                    min_x = min(min_x, x)
                    max_x = max(max_x, x)
                    min_y = min(min_y, y)
                    max_y = max(max_y, y)
        
        if len(Muro.muros) > 0:
            fondo.x = (max_x - min_x) / 2 * TAMANIO_TILE
            fondo.y = -((max_y - min_y) / 2 * TAMANIO_TILE)
            pilas.camara.x = fondo.x
            pilas.camara.y = fondo.y
    
    @staticmethod
    def evaluar_victoria():
        if len(Objetivo.objetivos) == 0:
            NulGame.nivel += 1
            if Nivel.niveles.has_key(NulGame.nivel):
                NulGame.limpiar_pantalla()
                NulGame.cargar_nivel()
            else:
                print('¡¡¡ V I C T O R I A !!!')
    
    @staticmethod
    def limpiar_pantalla():
        Muro.eliminar_todos()
        Movil.eliminar_todos()
        Objetivo.eliminar_todos()
                                            
class Nivel():
    
    niveles = {}
    
    @staticmethod
    def cargar_niveles():
        # Nivel 1
        nivel = []
        nivel.append('#############')
        nivel.append('#+         -#')
        nivel.append('#############')
        Nivel.niveles[0] = {}
        Nivel.niveles[0]['color'] = pilas.colores.azuloscuro
        Nivel.niveles[0]['color_fondo'] = pilas.colores.azul
        Nivel.niveles[0]['nivel'] = nivel
        
        # Nivel 2
        nivel = []
        nivel.append('#####')
        nivel.append('#+#-#')
        nivel.append('#   #')
        nivel.append('#   #')
        nivel.append('#####')
        Nivel.niveles[1] = {}
        Nivel.niveles[1]['color'] = pilas.colores.rojo
        Nivel.niveles[1]['color_fondo'] = pilas.colores.rosa
        Nivel.niveles[1]['nivel'] = nivel
        
        # Nivel 3
        nivel = []
        nivel.append('###    ##### ###')
        nivel.append('# #  ###   # # #')
        nivel.append('# #  #+    # # #')
        nivel.append('# #  ### ### # #')
        nivel.append('#+#    #-#   #+#')
        nivel.append('###    ###   ###')
        Nivel.niveles[2] = {}
        Nivel.niveles[2]['color'] = pilas.colores.verdeoscuro
        Nivel.niveles[2]['color_fondo'] = pilas.colores.verde
        Nivel.niveles[2]['nivel'] = nivel
        
        # Nivel 4
        nivel = []
        nivel.append('#####')
        nivel.append('# - #')
        nivel.append('#  +#')
        nivel.append('#  ##')
        nivel.append('####')
        Nivel.niveles[3] = {}
        Nivel.niveles[3]['color'] = pilas.colores.naranja
        Nivel.niveles[3]['color_fondo'] = pilas.colores.amarillo
        Nivel.niveles[3]['nivel'] = nivel
        
        # Nivel 5
        nivel = []
        nivel.append('  #####  ')
        nivel.append(' ##   ###')
        nivel.append(' #     -#')
        nivel.append(' #     ##')
        nivel.append('##      #')
        nivel.append('#  +#   #')
        nivel.append('#########')
        Nivel.niveles[4] = {}
        Nivel.niveles[4]['color'] = pilas.colores.violeta
        Nivel.niveles[4]['color_fondo'] = pilas.colores.azuloscuro
        Nivel.niveles[4]['nivel'] = nivel
        
                                                                                                                                                                                  
pilas.actores.vincular(Movil)
pilas.actores.vincular(Muro)
pilas.actores.vincular(Objetivo)

Nivel.cargar_niveles()
NulGame.cargar_nivel()

pilas.ejecutar()
